
### This project contains examples of how to do certain things in CMake that were not immediately obvious to me.

Some of this is more verbose/redundant than what I'd actually use in a project.
In some cases this makes it clearer, in others maybe not.

Things you'll find and where:
* Windows/unix symbol visibility macros are in libA and libB
* Generated files which are part of compilation are in libB
* Generated files which are not compiled (and thus require special handling) are in libC
* Creating 64 bit builds on windows is implemented in the build.py script

Thing that confused me, but isn't shown in these examples:
* Mixed c/c++ library was documented in libD (now removed). Originally didn't work as expected because project(D CXX) stops cmake from compiling C. Then encountered failures when trying to use symbols from the mixed c/c++ library in a c++ executable, We needed to wrap the inclusion of header.h in an extern "C" block. Since this test was exploring what to do with a library containing c & c++ that we didn't want to modify, I just decided to completely remove libD and compile everything as c++ (only on windows) for the other library. We know that that works, because it's how we'd been doing it in scons.