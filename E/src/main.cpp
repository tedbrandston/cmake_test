
#include <iostream>

#include "A/header.hpp"
#include "B/header.hpp"
#include "B/generated_header.hpp"
#include "C/gen_header.hpp"

int main() {
    std::cout << A::used_by_E_only() << std::endl;
    std::cout << A::used_by_B_and_E() << std::endl;
    std::cout << B::used_by_E_uses_A() << std::endl;
    std::cout << B::used_by_E() << std::endl;
    std::cout << B::generated() << std::endl;
    std::cout << C::generated() << std::endl;
};
