
import argparse
import os
import re

def header_guard(path):
    return re.sub('[-.]', '_', os.path.basename(path)).upper() + '_'

def write_files(header):
    # ensure output directory exists
    path = os.path.dirname(header)
    if not os.path.exists(path):
        os.makedirs(path)

    guard = header_guard(header)
    header_contents = '\n'.join([
        '',
        '#ifndef ' + guard,
        '#define ' + guard,
        '',
        '#include <string>',
        '',
        'namespace C {',
        'std::string generated() {',
        '  return "generated in C";',
        '}',
        '}  // namespace C',
        '',
        '#endif  // ' + guard,
        ''
    ])

    with open(header, 'w') as file:
        file.write(header_contents)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generates a header file')

    parser.add_argument(
        '--header',
        help='The path where the header should be generated')

    args = parser.parse_args()

    write_files(args.header)
