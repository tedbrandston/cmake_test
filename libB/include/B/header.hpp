
#ifndef INCLUDE_B_HEADER_H_
#define INCLUDE_B_HEADER_H_

#include <string>

namespace B {
B_API std::string used_by_E_uses_A();
B_API std::string used_by_E();
}  // namespace B
#endif  // INCLUDE_B_HEADER_H_
