
#include "A/header.hpp"
#include "B/header.hpp"

namespace B {
std::string used_by_E_uses_A() {
  return std::string("using A: ") + A::used_by_B_and_E() + A::used_by_B_only();
}

std::string used_by_E() {
  return "used by E";
}
}  // namespace B