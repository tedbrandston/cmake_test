
import argparse
import os
import re

def header_guard(path):
    return re.sub('[-.]', '_', os.path.basename(path)).upper() + '_'

def write_files(header, source):
    guard = header_guard(header)
    
    # ensure directories exist
    header_dir = os.path.dirname(header)
    source_dir = os.path.dirname(source)
    if not os.path.exists(header_dir):
        os.makedirs(header_dir)
    if not os.path.exists(source_dir):
        os.makedirs(source_dir)
    
    header_contents = '\n'.join([
        '',
        '#ifndef ' + guard,
        '#define ' + guard,
        '',
        '#include <string>',
        '',
        'namespace B {',
        'B_API std::string generated();',
        '}  // namespace B',
        '',
        '#endif  // ' + guard,
        ''
    ])

    source_contents = '\n'.join([
        '',
        '#include "' + header +'"',
        '',
        'namespace B {',
        'std::string generated() {',
        '  return "generated";',
        '}',
        '}  // namespace B',
        '',
    ])

    with open(header, 'w') as file:
        file.write(header_contents)
    with open(source, 'w') as file:
        file.write(source_contents)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generates a header/source pair')

    parser.add_argument(
        '--header',
        help='The path where the header should be generated')
    parser.add_argument(
        '--source',
        help='The path where the source should be generated')

    args = parser.parse_args()

    write_files(args.header, args.source)
