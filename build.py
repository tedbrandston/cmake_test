
import argparse
import os
import shutil
import subprocess
import sys

def get_build_dir(base_dir, x64):
    build_dir = os.path.join(base_dir, 'build')
    if x64:
        build_dir += '64'
    return build_dir

def build(base_dir, install_dir, x64):
    build_dir = get_build_dir(base_dir, x64)
    # if the build dir isn't present, cmake hasn't been run
    if not os.path.exists(build_dir):
        os.makedirs(build_dir)
        cmake_command = ['cmake']
        if x64:
            cmake_command += ['-G', 'Visual Studio 11 Win64']
        cmake_command.append('-DCMAKE_INSTALL_PREFIX=' + install_dir)
        cmake_command.append('..')
        rv = subprocess.check_call(cmake_command, cwd=build_dir)
        if rv != 0:
            return rv
    return subprocess.check_call([
        'cmake',
        '--build', build_dir,
        '--target', 'install'])

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=
        'Uses cmake to build a directory')

    parser.add_argument(
        'directory',
        help=
          'The directory to build.'
          ' The build files will be generated in a "build" subdir.')

    parser.add_argument(
        '--install-dir',
        default=os.path.abspath('install'),
        help='The directory in which to install build products')

    parser.add_argument(
        '-c',
        '--clean',
        action='store_true',
        help='delete the build directory')

    parser.add_argument(
        '--x64',
        action='store_true',
        help='Create a 64 bit build (windows only)'
            if sys.platform == 'win32' else argparse.SUPPRESS)

    args = parser.parse_args()

    if args.clean:
        shutil.rmtree(get_build_dir(args.directory, args.x64))
    else:
        sys.exit(build(args.directory, args.install_dir, args.x64))
