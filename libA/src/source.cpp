
#include "A/header.hpp"

namespace A {
std::string used_by_B_only() {
  return "used by B";
}

std::string used_by_E_only() {
  return "used by E";
}

std::string used_by_B_and_E() {
  return "used by B and E";
}
}  // namespace A
