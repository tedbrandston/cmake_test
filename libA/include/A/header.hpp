
#ifndef INCLUDE_A_HEADER_H_
#define INCLUDE_A_HEADER_H_

#include <string>

namespace A {
A_API std::string used_by_B_only();
A_API std::string used_by_E_only();
A_API std::string used_by_B_and_E();
}  // namespace A
#endif  // INCLUDE_A_HEADER_H_